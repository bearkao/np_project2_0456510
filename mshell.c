#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>

/* the maximum character in a single command line */
#define MAX_COMMAND 15000
/* the maximum character in a single command */
#define MAX_ARG 256
#define MAXREAD 15000 
#define MAX_SINGLE_LINE_CH 10000 
#define MAX_CLIENT 30
#define LISTENQ 1024

const char old_welcom_msg[] = "************************************************************** \n\
** Welcome to the information server, myserver.nctu.edu.tw. **\n\
**************************************************************\n\
** You are in the directory, /home/studentA/ras/.\n\
** This directory will be under \"/\", in this system.\n\
** This directory includes the following executable programs.\n\
**\n\
**  bin/\n\
**  test.html   (test file)\n\
**\n\
** The directory bin/ includes: \n\
**  cat\n\
**  ls\n\
**  removetag       (Remove HTML tags.)\n\
**  removetag0      (Remove HTML tags with error message.)\n\
**  number          (Add a number in each line.)\n **\n\
** In addition, the following two commands are supported by ras. \n\
**  setenv  \n\
**  printenv \n\
**\n";

const char welcom_msg[] = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";
const char prompt_ch[] = "% ";

const char local_bin[] = "/Users/gaoduanting/Desktop/commands";
// /net/gcs/104/0456510/ras/bin
// /Users/gaoduanting/Desktop/commands
char g_input_command[MAX_COMMAND];
int g_has_g_pipe, g_has_g_err_pipe, g_pipe;
int DEBUG = 0;
int f = 1;
typedef struct lolpipe {
	int is_strer;
} Local_pipe;

typedef struct global_pipe {
	int pipe[2];
	int is_use;
} Global_pipe;

typedef struct global_client {
	int fd;
	int g_input_count; /* from 0 to 999 */
	char name[20]; /* clients name : max 20 character */
	char ip_port[20];
	/* store clients personal environment, max env = 100 character */
	char clients_env[100];
} Global_client;

typedef struct return_val_3 {
	int suc;
	int in;
	int out;
} Return_val_3;

Local_pipe g_local_pipe[MAX_COMMAND];
Global_pipe g_pipes[MAX_CLIENT][1000]; /* max 30 clients */
Global_pipe public_pipe[100]; /* max 100 public pipe */
Global_client client[MAX_CLIENT];

int global_err_first;
char buf[MAX_SINGLE_LINE_CH]; // single line MAX output
char dup_input_command[MAXREAD];
int max_client = -1;

int first_time;

fd_set allset; 


int shell(int cli_index, int sock_fd, char* input_command); /* the main shell, return whether exit */
void init(); /* change directory */
void client_init(int cli_index); /* init client environment */
void convert_ip_port(struct sockaddr_in sock_in, char* ip_port);
int validation(char* start); /* validate input format */
void print_d(char*** p); /* debug print: print out command and its arg */
void initial_TCP_socket(struct sockaddr_in* servaddr, int* sock_fd, int p);
void close_global_pipe(int cli_index);
void handle_join(char* ip_port);
void handle_leave(int cli_index);
void handle_who(int cli_index, int sock_fd);
void handle_name(int cli_index, int sock_fd, char* new_name);
void handle_yell(int cli_index, char* words);
void handle_broadcast(char* mesg);
void handle_kick(int cli_index, int sock_fd, int n);
Return_val_3 handle_public_pipe(int cli_index, char** arg, int* out, int* in);
int is_digit(char* p);
int max_(int a, int b);

int main(int argc, char const *argv[], char** envp) {
	int port8080_fd, maxfd, connfd;
	int nready, i, j, n;
	char read_buf[MAXREAD];
	struct sockaddr_in servaddr_8080;
	socklen_t addr_len;
	fd_set rset; 

	initial_TCP_socket(&servaddr_8080, &port8080_fd, 8081); // port 8080
	maxfd = port8080_fd;
	FD_ZERO(&allset); 
	FD_SET(port8080_fd, &allset);
	init(); /* change directory */

	for(;;) {
		rset = allset;
		/* select will block here, waiting for incoming new client */
		// printf("before select\n");
		nready = select(maxfd+1, &rset, NULL, NULL, NULL);
		// printf("after select\n");
		if (FD_ISSET(port8080_fd, &rset)) { /* new client connect to 8080 */ 
			/* handle_8080_incoming_connection */
			addr_len = (socklen_t)sizeof(servaddr_8080);
			connfd = accept(port8080_fd,(struct sockaddr *)&servaddr_8080,&addr_len);
			for (i = 0; i < MAX_CLIENT; i++) {
				if (client[i].fd < 0) { 
					client[i].fd = connfd; /* save descriptor */ 
					break;
				}
			}
			if(i!=MAX_CLIENT) { 
				FD_SET(connfd, &allset); /* add new descriptor to set */
				if (connfd > maxfd)
					maxfd = connfd; /* for select */
				if(i>max_client)
					max_client = i;
				/* new client connct, send welcome msg */
				write(connfd, welcom_msg, strlen(welcom_msg));
				// write(connfd, prompt_ch, strlen(prompt_ch)+1);
				convert_ip_port(servaddr_8080, client[i].ip_port); /* store client ip and port */
				client_init(i); /* init client private global pipes and, env path */
				handle_join(client[i].ip_port);
				sprintf(buf, "%s", prompt_ch);
				write(connfd, buf, strlen(buf));
				if(--nready <= 0) /* no more readable descriptors */
					continue;
			}
			else {
				printf("too many clients\n");
				if(--nready<=0)
					continue; /* no more readable descriptors */
			}
		}
		for(i=0; i<=max_client; i++) { //
			if((connfd = client[i].fd) < 0)
				continue;
			if(FD_ISSET(connfd, &rset)) {
				/* read for client broadcast */
				if((n=read(connfd, read_buf, MAXREAD))==0) { /* connection closed by client */
					handle_leave(i);
					close(connfd);
					FD_CLR(connfd,&allset);
					client[i].fd = -1;
					close_global_pipe(i);                    
				} 
				else {
					/* todo: ?? is every client share a same directory ?? */
					/* todo: dont forget to change the clients environment */
					setenv("PATH", client[i].clients_env, 1); // set path
					/* todo: rewrite shell() function */
					strcpy(dup_input_command, read_buf);
					/* strip '\n' */
					if(dup_input_command[n-1]=='\n')
						dup_input_command[n-1] = '\0';
					if(dup_input_command[n-2]=='\r')
						dup_input_command[n-2] = '\0';
					// dup_input_command[strlen(dup_input_command)-2] = '\0';
					if(shell(i, connfd, read_buf)) { /* client leave */
						handle_leave(i);
						close(connfd); 
						FD_CLR(connfd,&allset);
						client[i].fd = -1;
						/* when client leave(using exit command), the cleanup of global pipe is needed */
						close_global_pipe(i);
					}
					memset(read_buf, '\0', n);
					/* todo: handle_join testing */
					/* todo: stdin stderr to socket */
					/* todo: implement public pipe(notice about Notice#5) */
					/* todo: add new support built-in commands */
				}                
				if(--nready <= 0)
					break; /* no more readable descriptors */
			}
		} // 
	}
	return 0;
}

void close_global_pipe(int cli_index) {
	int i;
	for(i=0; i<1000; i++) {
		if(g_pipes[cli_index][i].is_use) {
			close(g_pipes[cli_index][i].pipe[0]);
		}
	}
}

int max_(int a, int b) {
	if(a>b) {return a;}
	else {return b;}
}

void initial_TCP_socket(struct sockaddr_in* servaddr, int* sock_fd, int p) {
	*sock_fd = socket(AF_INET,SOCK_STREAM,0);
	bzero(servaddr, sizeof(struct sockaddr_in));
	servaddr->sin_family = AF_INET;
	servaddr->sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr->sin_port = htons(p);
	if(bind(*sock_fd,
		(struct sockaddr *)servaddr, sizeof(*servaddr))<0){
		perror("bind error");
		exit(-1);
	}   
	if(listen(*sock_fd,LISTENQ)<0) {
		perror("listen error\n");
		exit(-1);
	}   
}

char* find_command_pipe(char* start) {
	char* w;
	for(w=start; *w!='\0'; w++) {
		if(*w=='|' || *w=='!')
			return w;
	}
	return w;    
}

char* find_char(char* start, char target) {
	char* w;
	for(w=start; *w!='\0'; w++) {
		if(*w==target)
			return w;
	}
	return NULL;
}

void parse_command_arg(char* head, char* tail, char*** argv, int i) {
	char *w = head;
	int flag = 0, j = 0;
	while(*w==' ') { w++; }
	argv[i][0] = w;
	for(; w!=tail; w++) {
		if(*w=='\n' || *w=='\r') {
			*w = '\0';            
			break;
		}
		if(*w==' ') {
			*w = '\0';
			flag = 1;
		}
		else if(flag) {
			argv[i][++j] = w; 
			flag = 0;
		}
	}
	argv[i][++j] = NULL;
}

void print_d(char*** p) {
	int i,j;
	for(i=0; i<g_pipe+1; i++) {
		printf("command: %s\n", p[i][0] );
		for(j=1; j<MAX_ARG+1; j++) {
			if(p[i][j] == NULL)
				break;
			printf("%s\n", p[i][j]);
		}
	}
}

int fork_child_exec(char** argv, int fd_in, int fd_out, int fd_err) {
	int i;
	pid_t p = fork();
	if(p<0) { perror("error occur"); } // should never happen ??
	else if(p==0) { // child
		dup2(fd_in, 0);
		dup2(fd_out, 1);
		if(fd_err!= 0) { dup2(fd_err, 2); }
		// HOW-TO: close global pipe?? 
		if(execvp(argv[0], argv) == -1) {
			exit(errno);
		}
	}
	return 0;
}

void fork_redirct_pipe(int in, int in2, int out) {
	int n;
	pid_t p = fork();
	if(p<0) { perror("error occur"); } // should never happen ??
	else if(p==0) { // child
		dup2(out, 1);
		if(in!=0) {
			dup2(in,0);
			while(fgets(buf, sizeof(buf), stdin)!=NULL) {
				printf("%s", buf);
				fflush(stdout);
			}
		}
		if(in2!=0) {
			if(in!=0) {close(in);}
			if(dup2(in2, 0) == -1) {
				printf("dup error!!!!!!!!\n");
			}
			while(fgets(buf, sizeof(buf), stdin)!=NULL) {
				printf("%s", buf);
				fflush(stdout);             
			}
			// printf("child exit\n");
		}
		exit(0);
	}
}

/* TODO: take the g_input_count as para, return global pipe if exist */
int has_global_pipe(int cli_index, int c) {
	return g_pipes[cli_index][c].is_use ? g_pipes[cli_index][c].pipe[0] : 0;
}

void fork_redirct_pipe2(int cli_index, int in_pipe, int out_pipe, int target1, int target2) {
	int stat, n;
	if(target1>=1000)
		target1 -= 1000;

	n = has_global_pipe(cli_index, target1);    

	if(n!=0) { // here is really!!! weird!!!!???? why cant we pass three var??
		fork_redirct_pipe(n, 
			0, out_pipe);
		wait(&stat);
		fork_redirct_pipe(0, 
			target2, out_pipe);
	}
	else {
		fork_redirct_pipe(0, 
			target2, out_pipe);
	}

	close(out_pipe);
	close(target2);
	if(n > 0) { close(n); }
	wait(&stat);
	g_pipes[cli_index][target1].is_use = 1;
	g_pipes[cli_index][target1].pipe[0] = in_pipe;

}

int has_file_output(char** agv) {
	int i;
	for(i=0; agv[i]!=NULL; i++) {
		if(strcmp(">", agv[i])==0) {
			agv[i] = NULL;
			return i+1;
		}
	}
	return 0;
}

/* careful: max sending size: 10000 */
void handle_who(int cli_index, int sock_fd) {
	int i;
	char tmp_buf[50]; /* 50 should be sufficient */
	sprintf(buf, "%s\n", "<ID>	<nickname>	<IP/port>	<indicate me>");
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		if(i==cli_index)
			sprintf(tmp_buf, "%d	%s	%s	<-me\n", i+1, client[i].name, client[i].ip_port);
		else
			sprintf(tmp_buf, "%d	%s	%s\n", i+1, client[i].name, client[i].ip_port);
		strcat(buf, tmp_buf);
	}
	write(sock_fd, buf, strlen(buf));
}

void handle_tell(int cli_index, int sock_fd, char* target_id, char* words) {
	/* first check whether target exist */
	int id = atoi(target_id)-1;
	if(client[id].fd<0) {
		sprintf(buf, "*** Error: user #%d does not exist yet. ***\n", id+1);
		write(sock_fd, buf, strlen(buf));
		return;
	}
	sprintf(buf, "*** %s told you ***: %s\n", client[cli_index].name, words);
	write(client[id].fd, buf, strlen(buf));
}

void handle_yell(int cli_index, char* words) {
	int i;
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		sprintf(buf, "*** %s yelled ***: %s\n", client[cli_index].name, words);
		write(client[i].fd, buf, strlen(buf));
	}
}

void handle_join(char* ip_port) {
	int i;
	sprintf(buf, "*** User '(no name)' entered from %s. ***\n", ip_port);
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)	
			continue;
		write(client[i].fd, buf, strlen(buf));
	}
}

void handle_leave(int cli_index) {
	int i;
	sprintf(buf, "*** User '%s' left. ***\n", client[cli_index].name);
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		write(client[i].fd, buf, strlen(buf));
	}
}

int name_collision(char* new_name) {
	int i;
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		if(strcmp(new_name, client[i].name)==0)
			return 1;
	}
	return 0;
}

void handle_name(int cli_index, int sock_fd, char* new_name) {
	int i;
	/* check for name collision */
	if(name_collision(new_name)) {
		sprintf(buf, "*** User '%s' already exists. ***\n", new_name);
		write(sock_fd, buf, strlen(buf));
		return;
	}
	strcpy(client[cli_index].name, new_name);
	for(i=0; i<=max_client; i++) { /* boradcast to all user this info */
		if(client[i].fd<0)
			continue;
		sprintf(buf, "*** User from %s is named '%s'. ***\n", client[cli_index].ip_port, new_name);
		write(client[i].fd, buf, strlen(buf));
	}
}

void handle_broadcast(char* mesg) {
	int i;
	// char test_mesg[] = "hello";
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		write(client[i].fd, mesg, strlen(mesg));
	}
}

/* return whether public_in success */
Return_val_3 handle_public_pipe(int cli_index, char** arg, int* out, int* in) {
	int i, n, public_in=0, public_out=0;
	Return_val_3 return_val;
	return_val.suc = 1;
	return_val.in = 0;
	return_val.out = 0;
	char send_buf[100];
	for(i=1; (i<3)&&(arg[i]!=NULL); i++) { /* order is consider */
		if(arg[i][0]=='<' && is_digit(&arg[i][1])) {
			n = atoi(&arg[i][1]);
			if(public_pipe[n].is_use) { /* TODO: dont forget to close when fork process complete */
				*in = public_pipe[n].pipe[0];
				public_pipe[n].is_use = 0;
				return_val.in = public_pipe[n].pipe[0];
				public_in = 1;
			}
			else { /* error : not exist yet */ /* TODO: error handling */
				sprintf(
					buf, 
					"*** Error: public pipe #%d does not exist yet. ***\n", 
					n);
				write(client[cli_index].fd, buf, strlen(buf));
				return_val.suc = 0;
			}
			arg[i] = NULL;
		}
		else if(arg[i][0]=='>' && is_digit(&arg[i][1])) {
			n = atoi(&arg[i][1]);
			if(public_pipe[n].is_use) { /* error : TODO: error handling */
				return_val.suc = 0;
				sprintf(
					buf, 
					"*** Error: public pipe #%d already exists. ***\n", 
					n);
				write(client[cli_index].fd, buf, strlen(buf));
			}
			else {
				/* TODO: dont forget to close write pipe when fork process complete */
				int public_out_pipe[2];
				pipe(public_out_pipe);
				*out = public_out_pipe[1];
				return_val.out = public_out_pipe[1];
				public_pipe[n].pipe[0] = public_out_pipe[0];
				public_pipe[n].is_use = 1;
				public_out=1;
			}
			arg[i] = NULL;
		}
	}
	/* broadcast message to every client , <n first, then >n */
	if(public_in) {
		sprintf(buf, "*** %s (#%d) just received via '%s' ***\n", 
			client[cli_index].name, cli_index+1, dup_input_command);
		handle_broadcast(buf);
	}
	if(public_out) {
		sprintf(buf, "*** %s (#%d) just piped '%s' ***\n", 
			client[cli_index].name, cli_index+1, dup_input_command);		
		handle_broadcast(buf);		
	}
	return return_val;
}

int parse_commands(int cli_index, int sock_fd, char* start) {
	char* tok_head=start, *tok_tail;
	char* command_argv[g_pipe+1][MAX_ARG];
	char** argv[g_pipe+1];
	memset(command_argv, '\0', sizeof(command_argv));
	memset(argv, '\0', sizeof(argv));
	int g_pipe_fd[2], g_err_fd[2], first_g_pipe=0, second_g_pipe=0;
	int target1, target2, temp, stat, increment_command_count=1;
	int g_input_count = client[cli_index].g_input_count;
	pid_t pid;
	int return_code = 0;
	Return_val_3 public_pipe_return_val;

	if(g_has_g_pipe>0) {
		if(DEBUG) {printf("global pipe %d\n", g_has_g_pipe);}
		pipe(g_pipe_fd);
		first_g_pipe = g_pipe_fd[0];
		target1 = g_input_count + g_has_g_pipe;
	}

	if(g_has_g_err_pipe>0) {
		if(DEBUG) {printf("global err pope %d\n", g_has_g_err_pipe);}
		pipe(g_err_fd); 
		second_g_pipe = g_err_fd[0];
		target2 = g_input_count + g_has_g_err_pipe;
	}    

	if(global_err_first) {
		first_g_pipe = g_err_fd[0];
		second_g_pipe = g_pipe_fd[0];
		temp = target1;
		target1 = target2;
		target2 = temp;
	}

	/* parse command */
	int i,n;
	for(i=0; i<g_pipe+1; i++) {
		tok_tail = find_command_pipe(tok_head);
		if(*tok_tail=='!') { g_local_pipe[i].is_strer = 1; }
		else { g_local_pipe[i].is_strer = 0; }
		argv[i] = command_argv[i];
		parse_command_arg(tok_head, tok_tail, argv, i);
		tok_head = tok_tail+1;
	}

	int prev_fd[2], next_fd[2];
	prev_fd[0] = 0;
	/* execute */
	for(i=0; i<g_pipe+1; i++) {
		/* stdin, stdout and stderr change to sock_fd */
		int in=0, out=sock_fd, err=sock_fd;
		// stdin, stdout, stderr
		if(i==0 && ((n=has_global_pipe(cli_index, g_input_count))>0)) { // TODO
			if(DEBUG) {printf("%d test in n: %d\n", g_input_count , n);}
			in=n; 
		}
		else if(i==0 && (has_global_pipe(cli_index, g_input_count)==0)) {
			in = sock_fd; /* add */
		}
		else { 
			in = prev_fd[0];
		}

		// temporary write in a temp pipe, if no error, then redirect to global pipe, and global err pipe
		pipe(next_fd);
		if(i==g_pipe) { // TODO
			if(g_has_g_pipe>0) { out = g_pipe_fd[1]; }
			else { 
				out = sock_fd; /* add */
				err = sock_fd; /* add */
			}
			if(g_has_g_err_pipe>0) { err = g_err_fd[1]; }
		}
		else { 
			if(g_local_pipe[i].is_strer) {
				err = next_fd[1];
			}
			else {
				out = next_fd[1];
			}
		}

		// TODO: IO redirection
		if((n=has_file_output(argv[i]))>0) { 
			FILE* f = fopen(argv[i][n], "w");
			fclose(f);
			out = open(argv[i][n],O_WRONLY);
			if(out==-1) {
				perror("error");
			}
		}

		public_pipe_return_val=handle_public_pipe(cli_index, argv[i], &out, &in);

		/* TODO: public pipe */

		// TODO: create global pipe out and global error pipe out 
		// , and then merge to global pipe.
		if(public_pipe_return_val.suc) {
			fork_child_exec(argv[i], in, out, err); // TODO error handle (unknow command)
			if(public_pipe_return_val.in!=0)
				close(in);			
			if(public_pipe_return_val.out!=0)
				close(out);
			close(next_fd[1]); // after fork
			wait(&stat);
			stat = WEXITSTATUS(stat);
		}
		else {
			close(next_fd[1]); // after fork
			stat = 1; /* error occur when doing public pipe */			
		}
		if(DEBUG) {printf("child %d terminated: %d\n", pid, stat);}

		if(stat > 0) { // unknow comman (might be a built-in command)
			/* TODO: handle client leave(set sock fd to -1) */            
			if(strcmp(argv[i][0], "exit")==0) { /* built-in command */
				return_code = 1;
			}
			/* TODO: print client environment */
			else if(strcmp(argv[i][0], "printenv")==0) { /* built-in command */
				sprintf(buf, "%s=%s\n", argv[i][1], getenv(argv[i][1]));
				write(sock_fd, buf, strlen(buf));
			}
			/* TODO: set client private environment */
			else if(strcmp(argv[i][0], "setenv")==0) { /* built-in command */
				if(setenv(argv[i][1], argv[i][2], 1)==-1) {
					perror("set env error");
				}
				/* if set PATH, store it */
				if(strcmp(argv[i][1], "PATH")==0)
					strcpy(client[cli_index].clients_env, argv[i][2]);
			}
			else if(strcmp(argv[i][0], "who")==0) {
				handle_who(cli_index, sock_fd);
			}
			else if(strcmp(argv[i][0], "tell")==0) {
				char* w = find_char(dup_input_command+5, ' ');
				handle_tell(cli_index, sock_fd, argv[i][1], w+1);
			}
			else if(strcmp(argv[i][0], "yell")==0) {
				handle_yell(cli_index, dup_input_command+4);
			}
			else if(strcmp(argv[i][0], "name")==0) {
				handle_name(cli_index, sock_fd, argv[i][1]);
			}
			else if(strcmp(argv[i][0], "kick")==0) {
				handle_kick(cli_index, sock_fd, atoi(argv[i][1]));
			}
			else { /* not a built-in command(real unknow command) */
				if(err!=sock_fd) { /* change */
					if(g_has_g_err_pipe>0) {
						close(g_err_fd[1]);
						/* TODO: redirect pipe to sockfd, not stdout(not sure this function will work) */
						fork_redirct_pipe(g_err_fd[0],0,sock_fd);
					}
					else
						fork_redirct_pipe(next_fd[0],0,sock_fd); /* TODO: redirect pipe to sockfd, not stdout */
					wait(NULL);
				}
				/* TODO: write to sockfd */
				if(stat==ENOENT) {
					sprintf(buf, "Unknown command: [%s].\n", argv[i][0]); // stat-1
					write(sock_fd, buf, strlen(buf));
				}
				if(i==0) { // stat == 1
					increment_command_count = 0;
				}
				else { // TODO write to the next line command 
					/* important!!!!: it seems like we dont have to handle this */
					int temp_pipe[2];
					pipe(temp_pipe);
					fork_redirct_pipe2(cli_index, temp_pipe[0], temp_pipe[1], 
						g_input_count+1, prev_fd[0]);
					if((temp=has_global_pipe(cli_index, g_input_count))>0) {
						close(temp);
						g_pipes[cli_index][g_input_count].is_use = 0;
					}
				}
			}
			close(next_fd[0]);
			if(first_g_pipe!=0) { close(first_g_pipe); }
			if(second_g_pipe!=0) { close(second_g_pipe); }            
			if(n>0) {close(out);}
			break;
		}

		if(prev_fd[0]>0) { 
			close(prev_fd[0]); 
			prev_fd[0] = 0;
		}
		prev_fd[0] = next_fd[0];        

		if(n>0) {close(out);}
	} 
	if(DEBUG) { print_d(argv); }

	if(g_has_g_pipe>0) { close(g_pipe_fd[1]); }
	if(g_has_g_err_pipe>0) { close(g_err_fd[1]); }
	if(prev_fd[0]>0) { close(prev_fd[0]); }    
	/* wait child */

	if(stat==0 && (g_has_g_pipe>0 || g_has_g_err_pipe>0)) { // no error
		int temp_pipe[2], temp_pipe2[2];
		if((n=has_global_pipe(cli_index, g_input_count))>0) {
			g_pipes[cli_index][g_input_count].is_use = 0;
			close(n);
		}
		if(first_g_pipe!=0) {
			pipe(temp_pipe);
			fork_redirct_pipe2(cli_index, temp_pipe[0], temp_pipe[1], 
				target1, first_g_pipe);
		}            
		if(second_g_pipe!=0) {
			pipe(temp_pipe2);
			fork_redirct_pipe2(cli_index, temp_pipe2[0], temp_pipe2[1], 
				target2, second_g_pipe);                
		}
	}    

	if(increment_command_count) {
		g_input_count++;
		if(g_input_count==1000)
		   g_input_count = 0; // reset command counter        
	   client[cli_index].g_input_count = g_input_count;
	}

	return return_code;

	// TODO: global stderr pipe and global pipe
	// TODO: error command pipe still need to redirect
	// TODO: pipe after error command need to close
	// TODO: wait child prevent zombie 
	// TODO: handle command like: cat gg.txt (gg.txt is not exist)
}

int shell(int cli_index, int sock_fd, char* input_command) {
	int code, len;
	// printf("%s", "% "); // shell prompt, use read and write system call?? [%d], g_input_count
	// fflush(stdout);
	// fgets(g_input_command, sizeof(g_input_command), stdin);

	/*  
	** validate stdout and sterr only appear in the end of a line 
	** for emaple, "cat hello.txt |1 number |1 number |1" is an invalid command
	*/
	if((code=validation(input_command)) != 1) {
		printf("Invalid command %d\n", code);
		return 0;
	}

	/* parse command and execute, returen whether exit */
	code = parse_commands(cli_index, sock_fd, input_command);
	/* code==0, client is connecting, print prompt */
	if(!code) { 
		sprintf(buf, "%s", prompt_ch);
		write(sock_fd, buf, strlen(buf)); 
	}
	/* print prompt after finished execute command */
	
	return code;
}

int env_var(char** envp, char* var) {
	char** env;
	for (env = envp; *env != NULL; env++) {
	  char* thisEnv = *env;
	  if(strncmp(thisEnv, var, 4)==0)
		  printf("%s\n", thisEnv);
	}
	return 0;
}

void init() {
	int i;
	// if(chdir("/net/gcs/104/0456510/ras")!=0) {
	// 	perror("chdir error");
	// }
	if(chdir("./test_env/")!=0) {
		perror("chdir error");
	}
	for(i=0; i<MAX_CLIENT; i++) {
		client[i].fd = -1;
	}    
	for(i=0; i<100; i++) {
		public_pipe[i].is_use = 0;
	}
}

void convert_ip_port(struct sockaddr_in sock_in, char* ip_port) {
	strcpy(ip_port, "CGILAB/511");
	// sprintf(ip_port,"%d.%d.%d.%d/%d",
	// 	(int)(sock_in.sin_addr.s_addr&0xFF),
	// 	(int)((sock_in.sin_addr.s_addr&0xFF00)>>8),
	// 	(int)((sock_in.sin_addr.s_addr&0xFF0000)>>16),
	// 	(int)((sock_in.sin_addr.s_addr&0xFF000000)>>24),
	// 	ntohs(sock_in.sin_port));
}

void client_init(int cli_index) {
	int i;
	// setenv("PATH", "bin:.", 1); // set path
	/* TODO: remove all env except PATH ?? */
	strcpy(client[cli_index].clients_env, "bin:.");
	strcpy(client[cli_index].name, "(no name)");
	for(i=0; i<1000; i++) {
		g_pipes[cli_index][i].is_use = 0;
	}
	client[cli_index].g_input_count = 0;
}

int is_digit(char* p) {
	int num = *p - '0';
	if(num<0 || num>9)
		return 0;
	return 1;
}

int validation(char* start) {
	g_has_g_pipe = g_has_g_err_pipe = global_err_first = g_pipe = 0;
	char* head = start;
	char c = *head;
	char* num_p;
	int g_pipe_num=0;
	for(; *head!='\0'; head++, c=*head) {
		if(c=='|' || c=='!') {
			if(*(head+1) != ' ') { // | or ! should follow by a space, or a number
				if(is_digit(++head)) { // a global pipe, check this number
					num_p = head;
					while(is_digit(++head));
					if(*head!=' ' && *head!='\n' && *head!='\r') return 2; // invalid command 
					*head = '\0';
					g_pipe_num++; // only accept two consecutive pipes at the end of line
					if(c=='|') { g_has_g_pipe = atoi(num_p); } // has a global pipe
					if(c=='!') { // has a global error pipe
						if(g_pipe_num==1)
							global_err_first = 1; // only when there are two global pipe, set it
						g_has_g_err_pipe = atoi(num_p); 
					}

				}
				else return 3;
			}
			else { // | or ! follow by a new command
				if(g_pipe_num>=1) return 4; // a global pipe should not follow by a command 
				g_pipe++;
			}
		}
		if(g_pipe_num>=3) return 5;
	} 
	if(g_pipe_num==1 && global_err_first==1) { global_err_first = 0; }
	return 1;
}

void handle_kick(int cli_index, int sock_fd, int n) {
	handle_leave(n-1);
	close(client[n-1].fd);
	FD_CLR(client[n-1].fd,&allset);
	client[n-1].fd = -1;
	close_global_pipe(n-1);                    	
}

