#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sys/ipc.h>
#include <sys/shm.h>

/* the maximum character in a single command line */
#define MAX_COMMAND 15000
#define MAXREAD 15000 
#define MAX_CLIENT 30
/* the maximum character in a single command */
#define MAX_ARG 256
#define MAX_SINGLE_LINE_CH 10000 

const char old_welcom_msg[] = "************************************************************** \n\
** Welcome to the information server, myserver.nctu.edu.tw. **\n\
**************************************************************\n\
** You are in the directory, /home/studentA/ras/.\n\
** This directory will be under \"/\", in this system.\n\
** This directory includes the following executable programs.\n\
**\n\
**  bin/\n\
**  test.html   (test file)\n\
**\n\
** The directory bin/ includes: \n\
**  cat\n\
**  ls\n\
**  removetag       (Remove HTML tags.)\n\
**  removetag0      (Remove HTML tags with error message.)\n\
**  number          (Add a number in each line.)\n **\n\
** In addition, the following two commands are supported by ras. \n\
**  setenv  \n\
**  printenv \n\
**\n";

const char welcom_msg[] = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";

const char local_bin[] = "/Users/gaoduanting/Desktop/commands";
char prompt_ch[] = "% ";

// /net/gcs/104/0456510/ras/bin
// /Users/gaoduanting/Desktop/commands
char g_input_command[MAX_COMMAND];
int g_input_count = 0; // from 0 to 999 
int g_has_g_pipe, g_has_g_err_pipe, g_pipe;
char** g_envp;
int DEBUG = 0;
int f = 1;
int shmID, max_clientID;
int m_id, public_pipID;
typedef struct lolpipe {
    int is_strer;
} Local_pipe;

typedef struct global_client {
    pid_t pid;
    int fd;
    char name[20]; /* clients name : max 20 character */
    char ip_port[20];
    char mesg[1024];
} Global_client;

typedef struct global_pipe {
    int pipe[2];
    int is_use;
} Global_pipe;

typedef struct global_mesg {
    int is_use;
    char mesg[500]; 
} Global_mesg;

typedef struct return_val_3 {
    int suc;
    int in;
    int out;
} Return_val_3;

Global_client* client;
Global_pipe* public_pipe;

Local_pipe g_local_pipe[MAX_COMMAND];
Global_pipe g_pipes[1000];

int global_err_first;
char buf[MAX_SINGLE_LINE_CH]; // single line MAX output
char dup_input_command[MAXREAD];

int* mc;

int shell(); /* the main shell */
void init(char** envp); /* init path environment, and some var */
int validation(char* start); /* validate input format */
void print_d(char*** p); /* debug print: print out command and its arg */
void get_my_id();
int get_max_client();
void get_g_client();
void handle_who(int cli_index);
void handle_leave(int cli_index);
void handle_tell(int cli_index, char* target_id, char* words);
void handle_yell(int cli_index, char* words);
void handle_name(int cli_index, char* new_name);
void handle_broadcast(char* mesg);
void get_g_public_pipe();
Return_val_3 handle_public_pipe(int cli_index, char** arg, int* out, int* in);

void sig_child(int signo) {
    pid_t pid;
    int stat;
    pid = wait(&stat);
    printf("child %d terminated: %d\n", pid,  signo);
}

void when_sigusr1(int SIGCHLD_num) {
    printf("%s", client[m_id].mesg);
    fflush(stdout);    
}

int main(int argc, char const *argv[], char** envp) {

    shmID = atoi(argv[1]);
    max_clientID = atoi(argv[2]);
    public_pipID = atoi(argv[3]);
    m_id = atoi(argv[4]);

    signal(SIGUSR1,when_sigusr1);

    // printf("shmid: %d, mac_client: %d m_id: %d\n", shmID, max_clientID, m_id);
    g_envp = envp;
    init(envp); // change environment path
    mc = (int*)shmat(max_clientID,NULL,0);
    if((int)mc==-1)
        perror("client get mac_client error");
    get_g_client();
    get_g_public_pipe();

    // signal(SIGCHLD, sig_child);

    while(1) {
        // signal(SIGUSR1,when_sigusr1);
        if(shell()) {
            exit(0);            
        }
    }
    return 0;
}

char* find_command_pipe(char* start) {
    char* w;
    for(w=start; *w!='\0'; w++) {
        if(*w=='|' || *w=='!')
            return w;
    }
    return w;    
}

char* find_char(char* start, char target) {
    char* w;
    for(w=start; *w!='\0'; w++) {
        if(*w==target)
            return w;
    }
    return NULL;
}

void parse_command_arg(char* head, char* tail, char*** argv, int i) {
    char *w = head;
    int flag = 0, j = 0;
    while(*w==' ') { w++; }
    argv[i][0] = w;
    for(; w!=tail; w++) {
        if(*w=='\n' || *w=='\r') {
            *w = '\0';            
            break;
        }
        if(*w==' ') {
            *w = '\0';
            flag = 1;
        }
        else if(flag) {
            argv[i][++j] = w; 
            flag = 0;
        }
    }
    argv[i][++j] = NULL;
}

void print_d(char*** p) {
    int i,j;
    for(i=0; i<g_pipe+1; i++) {
        printf("command: %s\n", p[i][0] );
        for(j=1; j<MAX_ARG+1; j++) {
            if(p[i][j] == NULL)
                break;
            printf("%s\n", p[i][j]);
        }
    }
}

int fork_child_exec(char** argv, int fd_in, int fd_out, int fd_err) {
    int i;
    pid_t p = fork();
    if(p<0) { perror("error occur"); } // should never happen ??
    else if(p==0) { // child
        dup2(fd_in, 0);
        dup2(fd_out, 1);
        if(fd_err!= 0) { dup2(fd_err, 2); }
        // HOW-TO: close global pipe?? 
        if(execvp(argv[0], argv) == -1) {
            exit(errno);
        }
    }
    return 0;
}

void fork_redirct_pipe(int in, int in2, int out) {
    int n;
    pid_t p = fork();
    if(p<0) { perror("error occur"); } // should never happen ??
    else if(p==0) { // child
        dup2(out, 1);
        if(in!=0) {
            dup2(in,0);            
            while(fgets(buf, sizeof(buf), stdin)!=NULL) {
                printf("%s", buf);
                fflush(stdout);
            }
        }
        if(in2!=0) {
            if(in!=0) {close(in);}
            if(dup2(in2, 0) == -1) {
                printf("dup error!!!!!!!!\n");
            }
            while(fgets(buf, sizeof(buf), stdin)!=NULL) {
                printf("%s", buf);
                fflush(stdout);             
            }
            // printf("child exit\n");
        }
        exit(0);
    }
}

/* TODO: take the g_input_count as para, return global pipe if exist */
int has_global_pipe(int c) {
    return g_pipes[c].is_use ? g_pipes[c].pipe[0] : 0;
}

void fork_redirct_pipe2(int in_pipe, int out_pipe, int target1, int target2) {
    int stat, n;
    if(target1>=1000)
        target1 -= 1000;

    n = has_global_pipe(target1);    

    if(n!=0) { // here is really!!! weird!!!!???? why cant we pass three var??
        fork_redirct_pipe(n, 
            0, out_pipe);
        wait(&stat);
        fork_redirct_pipe(0, 
            target2, out_pipe);
    }
    else {
        fork_redirct_pipe(0, 
            target2, out_pipe);
    }

    close(out_pipe);
    close(target2);
    if(n > 0) { close(n); }
    wait(&stat);
    g_pipes[target1].is_use = 1;
    g_pipes[target1].pipe[0] = in_pipe;

}

int has_file_output(char** agv) {
    int i;
    for(i=0; agv[i]!=NULL; i++) {
        if(strcmp(">", agv[i])==0) {
            agv[i] = NULL;
            return i+1;
        }
    }
    return 0;
}

int parse_commands(char* start) {
    char* tok_head=start, *tok_tail;
    char* command_argv[g_pipe+1][MAX_ARG];
    char** argv[g_pipe+1];
    memset(command_argv, '\0', sizeof(command_argv));
    memset(argv, '\0', sizeof(argv));
    int g_pipe_fd[2], g_err_fd[2], first_g_pipe=0, second_g_pipe=0;
    int target1, target2, temp, stat, increment_command_count=1;
    pid_t pid;
    int return_code = 0;
    Return_val_3 public_pipe_return_val;


    if(g_has_g_pipe>0) {
        if(DEBUG) {printf("global pipe %d\n", g_has_g_pipe);}
        pipe(g_pipe_fd);
        first_g_pipe = g_pipe_fd[0];
        target1 = g_input_count + g_has_g_pipe;
    }

    if(g_has_g_err_pipe>0) {
        if(DEBUG) {printf("global err pope %d\n", g_has_g_err_pipe);}
        pipe(g_err_fd); 
        second_g_pipe = g_err_fd[0];
        target2 = g_input_count + g_has_g_err_pipe;
    }    

    if(global_err_first) {
        first_g_pipe = g_err_fd[0];
        second_g_pipe = g_pipe_fd[0];
        temp = target1;
        target1 = target2;
        target2 = temp;
    }

    /* parse command */
    int i,n;
    for(i=0; i<g_pipe+1; i++) {
        tok_tail = find_command_pipe(tok_head);
        if(*tok_tail=='!') { g_local_pipe[i].is_strer = 1; }
        else { g_local_pipe[i].is_strer = 0; }
        argv[i] = command_argv[i];
        parse_command_arg(tok_head, tok_tail, argv, i);
        tok_head = tok_tail+1;
    }

    int prev_fd[2], next_fd[2];
    prev_fd[0] = 0;
    /* execute */
    for(i=0; i<g_pipe+1; i++) {
        int in=0, out=1, err=0;
        // stdin, stdout, stderr
        if(i==0 && ((n=has_global_pipe(g_input_count))>0)) { // TODO
            if(DEBUG) {printf("%d test in n: %d\n", g_input_count , n);}
            in=n; 
        }
        else if(i==0 && (has_global_pipe(g_input_count)==0)) {
            in = 0;
        }
        else { 
            in = prev_fd[0];
        }

        // temporary write in a temp pipe, if no error, then redirect to global pipe, and global err pipe
        pipe(next_fd);
        if(i==g_pipe) { // TODO
            if(g_has_g_pipe>0) { out = g_pipe_fd[1]; }
            else { 
                out = 1; 
                err = 2;
            }
            if(g_has_g_err_pipe>0) { err = g_err_fd[1]; }
        }
        else { 
            if(g_local_pipe[i].is_strer) {
                err = next_fd[1];
            }
            else {
                out = next_fd[1];
            }
        }

        // TODO: IO redirection
        if((n=has_file_output(argv[i]))>0) { 
            FILE* f = fopen(argv[i][n], "w");
            fclose(f);
            out = open(argv[i][n],O_WRONLY);
            if(out==-1) {
                perror("error");
            }
        }

        public_pipe_return_val=handle_public_pipe(m_id, argv[i], &out, &in);


        // TODO: create global pipe out and global error pipe out 
        // , and then merge to global pipe.

        if(public_pipe_return_val.suc) {
            fork_child_exec(argv[i], in, out, err); // TODO error handle (unknow command)
            if(public_pipe_return_val.in!=0)
                close(in);          
            if(public_pipe_return_val.out!=0)
                close(out);
            close(next_fd[1]); // after fork
            wait(&stat);
            stat = WEXITSTATUS(stat);
        }
        else {
            close(next_fd[1]); // after fork
            stat = 1; /* error occur when doing public pipe */          
        }        
        if(DEBUG) {printf("child %d terminated: %d\n", pid, stat);}

        if(stat > 0) { // unknow comman
            if(strcmp(argv[i][0], "exit")==0) {
                handle_leave(m_id);                
                return_code = 1;
            }
            if(strcmp(argv[i][0], "printenv")==0) {
                // env_var(g_envp, argv[stat-1][1]);
                printf("%s=%s\n",argv[i][1], getenv(argv[i][1]));
            }
            else if(strcmp(argv[i][0], "setenv")==0) {
                if(setenv(argv[i][1], argv[i][2], 1)==-1) {
                    perror("set env error");
                }
            }
            else if(strcmp(argv[i][0], "who")==0) {
                handle_who(m_id);
            }
            else if(strcmp(argv[i][0], "tell")==0) {
                char* w = find_char(dup_input_command+5, ' ');
                handle_tell(m_id, argv[i][1], w+1);
            }
            else if(strcmp(argv[i][0], "yell")==0) {
                handle_yell(m_id, dup_input_command+4);
            }            
            else if(strcmp(argv[i][0], "name")==0) {
                handle_name(m_id, argv[i][1]);
            }            
            else {
                if(err!=2) {
                    if(g_has_g_err_pipe>0) {
                        close(g_err_fd[1]);
                        fork_redirct_pipe(g_err_fd[0],0,1);
                    }
                    else
                        fork_redirct_pipe(next_fd[0],0,1);
                    wait(NULL);
                }
                if(stat==ENOENT)
                    printf("Unknown command: [%s].\n", argv[i][0]); // stat-1
                if(i==0) { // stat == 1
                    increment_command_count = 0;
                }
                else { // TODO write to the next line command
                    int temp_pipe[2];
                    pipe(temp_pipe);
                    fork_redirct_pipe2(temp_pipe[0], temp_pipe[1], 
                        g_input_count+1, prev_fd[0]);
                    if((temp=has_global_pipe(g_input_count))>0) {
                        close(temp);
                        g_pipes[g_input_count].is_use = 0;
                    }
                }
            }
            close(next_fd[0]);
            if(first_g_pipe!=0) { close(first_g_pipe); }
            if(second_g_pipe!=0) { close(second_g_pipe); }            
            if(n>0) {close(out);}
            break;
        }

        if(prev_fd[0]>0) { close(prev_fd[0]); }
        prev_fd[0] = next_fd[0];        

        if(n>0) {close(out);}
    } 
    if(DEBUG) { print_d(argv); }

    if(g_has_g_pipe>0) { close(g_pipe_fd[1]); }
    if(g_has_g_err_pipe>0) { close(g_err_fd[1]); }
    if(prev_fd[0]>0) { close(prev_fd[0]); }    

    /* wait child */

    if(stat==0 && (g_has_g_pipe>0 || g_has_g_err_pipe>0)) { // no error
        int temp_pipe[2], temp_pipe2[2];
        if((n=has_global_pipe(g_input_count))>0) {
            g_pipes[g_input_count].is_use = 0;
            close(n);
        }
        if(first_g_pipe!=0) {
            pipe(temp_pipe);
            fork_redirct_pipe2(temp_pipe[0], temp_pipe[1], 
                target1, first_g_pipe);
        }            
        if(second_g_pipe!=0) {
            pipe(temp_pipe2);
            fork_redirct_pipe2(temp_pipe2[0], temp_pipe2[1], 
                target2, second_g_pipe);                
        }
    }    

    if(increment_command_count) {
        g_input_count++;
        if(g_input_count==1000)
           g_input_count = 0; // reset command counter        
    }

    return return_code;

    // TODO: global stderr pipe and global pipe
    // TODO: error command pipe still need to redirect
    // TODO: pipe after error command need to close
    // TODO: wait child prevent zombie 
    // TODO: handle command like: cat gg.txt (gg.txt is not exist)
}

int shell() {
    int code, len;
    fgets(g_input_command, sizeof(g_input_command), stdin);

    strcpy(dup_input_command, g_input_command);
    len = strlen(dup_input_command);

    /* strip '\n' */
    if(dup_input_command[len-1]=='\n')
        dup_input_command[len-1] = '\0';
    if(dup_input_command[len-2]=='\r')
        dup_input_command[len-2] = '\0';

    /*  
    ** validate stdout and sterr only appear in the end of a line 
    ** for emaple, "cat hello.txt |1 number |1 number |1" is an invalid command
    */
    if((code=validation(g_input_command)) != 1) {
        printf("Invalid command %d\n", code);
        return 0;
    }

    code = parse_commands(g_input_command);
    if(!code) {
        sprintf(buf, "%s", prompt_ch);
        printf("%s", buf);
        fflush(stdout);
    }

    return code;
    /* TODO: read from global pipes */

}

int env_var(char** envp, char* var) {
    char** env;
    for (env = envp; *env != NULL; env++) {
      char* thisEnv = *env;
      if(strncmp(thisEnv, var, 4)==0)
          printf("%s\n", thisEnv);
    }
    return 0;
}

void init(char** envp) {
    int i;
    // if(chdir("/net/gcs/104/0456510/ras")!=0) {
    //     perror("chdir error");
    // }
    if(chdir("./test_env/")!=0) {
        perror("chdir error");
    }
    // print_env(envp);
    setenv("PATH", "bin:.", 1); // set path
    // printf("update: %s\n", getenv("PATH")); // get path
    for(i=0; i<1000; i++) {
        g_pipes[i].is_use = 0;
    }
}

int is_digit(char* p) {
    int num = *p - '0';
    if(num<0 || num>9)
        return 0;
    return 1;
}

int validation(char* start) {
    g_has_g_pipe = g_has_g_err_pipe = global_err_first = g_pipe = 0;
    char* head = start;
    char c = *head;
    char* num_p;
    int g_pipe_num=0;
    for(; *head!='\0'; head++, c=*head) {
        if(c=='|' || c=='!') {
            if(*(head+1) != ' ') { // | or ! should follow by a space, or a number
                if(is_digit(++head)) { // a global pipe, check this number
                    num_p = head;
                    while(is_digit(++head));
                    if(*head!=' ' && *head!='\n' && *head!='\r') return 2; // invalid command 
                    *head = '\0';
                    g_pipe_num++; // only accept two consecutive pipes at the end of line
                    if(c=='|') { g_has_g_pipe = atoi(num_p); } // has a global pipe
                    if(c=='!') { // has a global error pipe
                        if(g_pipe_num==1)
                            global_err_first = 1; // only when there are two global pipe, set it
                        g_has_g_err_pipe = atoi(num_p); 
                    }

                }
                else return 3;
            }
            else { // | or ! follow by a new command
                if(g_pipe_num>=1) return 4; // a global pipe should not follow by a command 
                g_pipe++;
            }
        }
        if(g_pipe_num>=3) return 5;
    } 
    if(g_pipe_num==1 && global_err_first==1) { global_err_first = 0; }
    return 1;
}

int get_max_client() {
    int* mc = (int*)shmat(max_clientID,NULL,0);
    if((int)mc==-1)
        perror("client get mac_client error");
    // shmdt(mc);
    // printf("test1\n");
    // fflush(stdout);
    /* why shmdt not work ?? */
    return *mc;
}

void get_g_client() {
    client = (Global_client*)shmat(shmID,NULL,0);
    if((int)client==-1)
        perror("client get g_client error");
}

void get_g_public_pipe() {
    public_pipe = (Global_pipe*)shmat(public_pipID,NULL,0); 
    if((int)public_pipe==-1)
        perror("client get g_public_pipe error");    
}

void dt_g_client() {
    shmdt(client);
}

void handle_who(int cli_index) {
    printf("cli handle who\n");
    fflush(stdout);
    int i, max_client = *mc;
    char tmp_buf[50]; /* 50 should be sufficient */
    sprintf(buf, "%s\n", "<ID>  <nickname>  <IP/port>   <indicate me>");

    for(i=0; i<=max_client; i++) {
        if(client[i].fd<0)
            continue;
        if(i==cli_index)
            sprintf(tmp_buf, "%d    %s  %s  <-me\n", i+1, client[i].name, client[i].ip_port);
        else
            sprintf(tmp_buf, "%d    %s  %s\n", i+1, client[i].name, client[i].ip_port);
        strcat(buf, tmp_buf);
    }
    printf("%s", buf);
}

void handle_leave(int cli_index) {
    sprintf(buf, "*** User '%s' left. ***\n", client[cli_index].name);
    printf("%s", buf);
    exit(0);
}

void handle_tell(int cli_index, char* target_id, char* words) {
    /* first check whether target exist */
    int id = atoi(target_id)-1;
    // get_g_client();
    if(client[id].fd<0) {
        sprintf(buf, "*** Error: user #%d does not exist yet. ***\n", id+1);
        printf("%s", buf);
        return;
    }
    sprintf(buf, "*** %s told you ***: %s\n", client[cli_index].name, words);
    // printf("client test: %s, target pid: %d target ip: %s\n", buf, (int)client[id].pid, client[id].ip_port);
    /* write to target buf */
    strcpy(client[id].mesg, buf);
    /* TODO: signal target, not working?? when using TAs client.c */
    kill(client[id].pid, SIGUSR1);
}

void handle_yell(int cli_index, char* words) {
    int i, max_client = *mc;
    // get_g_client();
    sprintf(buf, "*** %s yelled ***: %s\n", client[cli_index].name, words);
    for(i=0; i<=max_client; i++) {
        if(client[i].fd<0)
            continue;
        strcpy(client[i].mesg, buf);
        kill(client[i].pid, SIGUSR1);
    }
}

int name_collision(char* new_name, int max_client) {
    int i;
    for(i=0; i<=max_client; i++) {
        if(client[i].fd<0)
            continue;
        if(strcmp(new_name, client[i].name)==0)
            return 1;
    }
    return 0;
}

void handle_name(int cli_index, char* new_name) {
    int i, max_client = *mc;
    /* check for name collision */
    if(name_collision(new_name, max_client)) {
        sprintf(buf, "*** User '%s' already exists. ***\n", new_name);
        printf("%s", buf);
        // write(sock_fd, buf, strlen(buf));
        return;
    }
    strcpy(client[cli_index].name, new_name);
    sprintf(buf, "*** User from %s is named '%s'. ***\n", client[cli_index].ip_port, new_name);
    for(i=0; i<=max_client; i++) { /* boradcast to all user this info */
        if(client[i].fd<0)
            continue;
        strcpy(client[i].mesg, buf);
        kill(client[i].pid, SIGUSR1);        
        // write(client[i].fd, buf, strlen(buf));
    }
}

Return_val_3 handle_public_pipe(int cli_index, char** arg, int* out, int* in) {
    int i, n, public_in=0, public_out=0;
    Return_val_3 return_val;
    return_val.suc = 1;
    return_val.in = 0;
    return_val.out = 0;
    char send_buf[100];
    char rwfifo[10];
    for(i=1; (i<3)&&(arg[i]!=NULL); i++) { /* order is consider */
        if(arg[i][0]=='<' && is_digit(&arg[i][1])) {
            n = atoi(&arg[i][1]);
            if(public_pipe[n].is_use) { /* TODO: dont forget to close when fork process complete */
                sprintf(rwfifo, "fifo.%d", n);
                *in = open(rwfifo, O_RDONLY);
                if(*in<0) {
                    perror("in error");
                    printf("on fifo %s\n", rwfifo);
                }
                printf("public pipe in %d\n", *in);
                // *in = public_pipe[n].pipe[0];
                public_pipe[n].is_use = 0;
                // return_val.in = public_pipe[n].pipe[0];
                return_val.in = *in;
                public_in = 1;
            }
            else { /* error : not exist yet */ /* TODO: error handling */
                sprintf(
                    buf, 
                    "*** Error: public pipe #%d does not exist yet. ***\n", 
                    n);
                printf("%s", buf);
                fflush(stdout);
                // write(client[cli_index].fd, buf, strlen(buf));
                return_val.suc = 0;
            }
            arg[i] = NULL;
        }
        else if(arg[i][0]=='>' && is_digit(&arg[i][1])) {
            n = atoi(&arg[i][1]);
            if(public_pipe[n].is_use) { /* error : TODO: error handling */
                return_val.suc = 0;
                sprintf(
                    buf, 
                    "*** Error: public pipe #%d already exists. ***\n", 
                    n);
                printf("%s", buf);
                // write(client[cli_index].fd, buf, strlen(buf));
            }
            else {
                /* TODO: dont forget to close write pipe when fork process complete */
                // int public_out_pipe[2];
                // pipe(public_out_pipe);
                sprintf(rwfifo, "fifo.%d", n);
                // strcpy(rwfifo, "fifo.1");
                *out = open(rwfifo, O_WRONLY);
                if(*out<0) {
                    perror("out error");
                    printf("on fifo %s\n", rwfifo);
                }
                printf("public pipe out %d\n", *out);
                // *out = public_out_pipe[1];
                // return_val.out = public_out_pipe[1];
                return_val.out = *out;                
                // public_pipe[n].pipe[0] = public_out_pipe[0];
                public_pipe[n].is_use = 1;
                public_out=1;
            }
            arg[i] = NULL;
        }
    }
    /* broadcast message to every client , <n first, then >n */
    if(public_in) {
        sprintf(buf, "*** %s (#%d) just received via '%s' ***\n", 
            client[cli_index].name, cli_index+1, dup_input_command);
        handle_broadcast(buf);
    }
    if(public_out) {
        sprintf(buf, "*** %s (#%d) just piped '%s' ***\n", 
            client[cli_index].name, cli_index+1, dup_input_command);        
        handle_broadcast(buf);      
    }
    return return_val;
}

void handle_broadcast(char* mesg) {
    int i, max_client = *mc;
    // char test_mesg[] = "hello";
    for(i=0; i<=max_client; i++) {
        if(client[i].fd<0)
            continue;
        strcpy(client[i].mesg, buf);
        kill(client[i].pid, SIGUSR1);
        // write(client[i].fd, mesg, strlen(mesg));
    }
}
