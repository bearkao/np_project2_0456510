#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h> 
#include <sys/wait.h>
#include <signal.h> 
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <fcntl.h>

#define MAX_CLIENT 30
#define MAXLINE 1000
#define LISTENQ 1024
#define MAXREAD 15000

const char welcom_msg[] = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";

typedef struct global_client {
	pid_t pid;
	int fd;
	char name[20]; /* clients name : max 20 character */
	char ip_port[20];
	char mesg[1024];
} Global_client;

typedef struct global_pipe {
    int pipe[2];
	int is_use;
} Global_pipe;

typedef struct global_mesg {
	int is_use;
	char mesg[500];	
} Global_mesg;

int shmID;
int max_clientID;
int public_pipID;
int max_client = -1;
char buf[MAXREAD];

void init();
void convert_ip_port(struct sockaddr_in sock_in, char* ip_port);
void handle_join(Global_client* client, char* ip_port, int max_client, int ig);

void handle_siguser1(int SIGCHLD_num) {
	printf("server recv sig\n");	
	Global_client* client = (Global_client*)shmat(shmID,NULL,0);
	kill(client[MAX_CLIENT].pid, SIGUSR1);
	printf("sig child %d\n", (int)client[MAX_CLIENT].pid);
	shmdt(client);
}

/* handle client leave */
void sig_chld(int signo) {
	pid_t pid;
	int stat, i;
	pid = wait(&stat);
	printf("child %d terminated\n", pid);
	Global_client* client = (Global_client*)shmat(shmID,NULL,0);
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
		if(client[i].pid==pid) {
			client[i].fd = -1;
		    sprintf(buf, "*** User '%s' left. ***\n", client[i].name);
		    break;
		}
	}
	/* broadcast leave mesg */
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0)
			continue;
        strcpy(client[i].mesg, buf);
        kill(client[i].pid, SIGUSR1);        		
		// write(client[i].fd, buf, strlen(buf));
	}
	shmdt(client);
}

/* press ^Z */
void sigtstp_handler(int signo) {
	printf("server leave!\n");
	/* cleanup share mem */
	shmctl(shmID, IPC_RMID, NULL);
	shmctl(max_clientID, IPC_RMID, NULL);
	exit(0);
}

int main(int argc, char **argv) {
	int listenfd, connfd, i;
	pid_t childpid;
	socklen_t clilen;
	struct sockaddr_in cliaddr, servaddr;
	int status;
	pid_t wpid;
	key_t shmKey = 45651, shmMClientKey = 22825;

	listenfd = socket(AF_INET, SOCK_STREAM,0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); // for any interface
	servaddr.sin_port = htons (8081); //user define

	if(bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr))<0){
		perror("bind error");
		return -1;
	}

	if(listen(listenfd,LISTENQ)<0) {
		printf("listen error\n");
		return -1;	
	} // LISTENQ=1024	

	shmID = shmget(IPC_PRIVATE, sizeof(Global_client)*(MAX_CLIENT+1), IPC_CREAT | 0666); // IPC_EXCL
	if(shmID == -1) {
		perror("est share mem error");
		exit(-1);
	}	
	printf("server shmID: %d\n", shmID);
	max_clientID = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666);
	if(max_clientID == -1) {
		printf("est share mem mclient error\n");
		exit(-1);
	}	
	printf("server max_clientID: %d\n", max_clientID);
	public_pipID = shmget(IPC_PRIVATE, sizeof(Global_pipe)*100, IPC_CREAT | 0666);
	if(public_pipID == -1) {
		printf("est share mem public_pipe error\n");
		exit(-1);
	}	
	printf("server public_pipID: %d\n", public_pipID);	

	init();

	/* set signal handler */
	/* from signal table: death of a child process */
	signal(SIGCHLD, sig_chld);	
	/* handle server leave */
	signal(SIGTSTP, sigtstp_handler);
	signal(SIGINT, sigtstp_handler);
	// signal(SIGUSR1, handle_siguser1);

	for ( ; ; ) {
		clilen = sizeof(cliaddr);
		connfd = accept(listenfd,(struct sockaddr *)&cliaddr,&clilen);

		Global_client* client = (Global_client*)shmat(shmID,NULL,0);
		if((int)client==-1)
			perror("server client");
		for (i = 0; i < MAX_CLIENT; i++) {
			if (client[i].fd < 0) { 
				client[i].fd = connfd; /* save descriptor */ 
				break;
			}
		}
		if(i>max_client)
			max_client = i;			
		strcpy(client[i].name, "(no name)");
		convert_ip_port(cliaddr, client[i].ip_port);
		int* mc = (int*)shmat(max_clientID,NULL,0);
		if((int)mc==-1)
			perror("mc");
		*mc = max_client;
		shmdt(mc);

		sprintf(buf, "*** User '(no name)' entered from %s. ***\n", client[i].ip_port);
		strcat(buf, "% ");
		write(connfd, welcom_msg, strlen(welcom_msg));
		write(connfd, buf, strlen(buf));

		if ( (childpid = fork()) == 0) { /* child process */
			printf("client connected!!\n");
			printf("In child process (pid = %d)\n", (int)getpid());
			close(listenfd); // close listening socket 
			char sshmID[10], smax_clientID[10], si[10], spublicpID[10];
			sprintf(sshmID, "%d", shmID);
			sprintf(smax_clientID, "%d", max_clientID);
			sprintf(si, "%d", i);
			sprintf(spublicpID, "%d", public_pipID);
			char* arg[] = {"./mshell", sshmID, smax_clientID, spublicpID, si, NULL};
			dup2(connfd, 0);
			dup2(connfd, 1);
			dup2(connfd, 2);
			if(execvp("./mshell", arg)==-1) {
				perror("error");
			}
			exit(0);
		} //end child process
		else {
			client[i].pid = childpid;
			handle_join(client, client[i].ip_port, max_client, i);
			printf("assign client %d pid %d\n", i, (int)childpid);
			shmdt(mc);
		}
		close(connfd); /* parent closes connected socket */
	}
} 
void init() {
	int i;
	Global_client* client = (Global_client*)shmat(shmID,NULL,0);
	if((int)client==-1)
		perror("server client");
	for(i=0; i<MAX_CLIENT; i++) {
		client[i].fd = -1;
	}    
	shmdt(client);
	Global_pipe* public_pipe = (Global_pipe*)shmat(public_pipID,NULL,0);
	char n[10];
	for(i=0; i<100; i++) {
		public_pipe[i].is_use = 0;
		/* fifo not working ... */
		sprintf(n, "fifo.%d", i);
		printf("making fifo %s\n", n);
		if(mknod(n, S_IFIFO | 0666, 0)<0)
			perror("mkfifo error");
		// if (mkfifo(n, 0666) < 0) {    create a fifo 
		// 	perror("mkfifo failed");
		// }		
	}
	shmdt(public_pipe);

}

void convert_ip_port(struct sockaddr_in sock_in, char* ip_port) {
	sprintf(ip_port,"%d.%d.%d.%d/%d",
		(int)(sock_in.sin_addr.s_addr&0xFF),
		(int)((sock_in.sin_addr.s_addr&0xFF00)>>8),
		(int)((sock_in.sin_addr.s_addr&0xFF0000)>>16),
		(int)((sock_in.sin_addr.s_addr&0xFF000000)>>24),
		ntohs(sock_in.sin_port));
}

void handle_join(Global_client* client, char* ip_port, int max_client, int ig) {
	int i;
	sprintf(buf, "*** User '(no name)' entered from %s. ***\n", ip_port);
	for(i=0; i<=max_client; i++) {
		if(client[i].fd<0 || ig==i)
			continue;
        strcpy(client[i].mesg, buf);
        printf("sig pid %d\n", (int)client[i].pid);
        kill(client[i].pid, SIGUSR1);
		// write(client[i].fd, buf, strlen(buf));
	}
}



