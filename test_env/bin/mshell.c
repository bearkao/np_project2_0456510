#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>


/* the maximum character in a single command line */
#define MAX_COMMAND 15000
/* the maximum character in a single command */
#define MAX_ARG 256
#define MAX_SINGLE_LINE_CH 1000 

const char welcom_msg[] = "************************************************************** \n\
** Welcome to the information server, myserver.nctu.edu.tw. **\n\
**************************************************************\n\
** You are in the directory, /home/studentA/ras/.\n\
** This directory will be under \"/\", in this system.\n\
** This directory includes the following executable programs.\n\
**\n\
**  bin/\n\
**  test.html   (test file)\n\
**\n\
** The directory bin/ includes: \n\
**  cat\n\
**  ls\n\
**  removetag       (Remove HTML tags.)\n\
**  removetag0      (Remove HTML tags with error message.)\n\
**  number          (Add a number in each line.)\n\
**\n\
** In addition, the following two commands are supported by ras. \n\
**  setenv  \n\
**  printenv \n\
**\n";
const char local_bin[] = "/Users/gaoduanting/Desktop/commands";
char g_input_command[MAX_COMMAND];
int g_input_count = 0; // from 0 to 999 
int g_has_g_pipe, g_has_g_err_pipe, g_pipe;
int DEBUG = 0;
typedef struct lolpipe {
    int pipe[2];
    int is_strer;
} Local_pipe;

typedef struct global_pipe {
    int pipe[2];
    int is_use;
} Global_pipe;

Local_pipe g_local_pipe[MAX_COMMAND];
Global_pipe g_pipes[1000];

int fd[2], global_err_first;
char buf[MAX_SINGLE_LINE_CH]; // single line MAX output

void shell(); /* the main shell */
int init(char** envp); /* init path environment, and some var */
int validation(char* start); /* validate input format */
void print_d(char*** p); /* debug print: print out command and its arg */

void sig_child(int signo) {
    pid_t pid;
    int stat;
    pid = wait(&stat);
    printf("child %d terminated: %d\n", pid,  signo);
}

int main(int argc, char const *argv[], char** envp) {
    printf("%s", welcom_msg);
    init(envp); // change environment path
    // signal(SIGCHLD, sig_child);

    while(1) {
        shell();
    }
    return 0;
}

char* find_command_pipe(char* start) {
    char* w;
    for(w=start; *w!='\0'; w++) {
        if(*w=='|' || *w=='!')
            return w;
    }
    return w;    
}

char* find_char(char* start, char target) {
    char* w;
    for(w=start; *w!='\0'; w++) {
        if(*w==target)
            return w;
    }
    return NULL;
}

void parse_command_arg(char* head, char* tail, char*** argv, int i) {
    char *w = head;
    int flag = 0, j = 0;
    while(*w==' ') { w++; }
    argv[i][0] = w;
    for(; w!=tail; w++) {
        if(*w=='\n') {
            *w = '\0';            
            break;
        }
        if(*w==' ') {
            *w = '\0';
            flag = 1;
        }
        else if(flag) {
            argv[i][++j] = w; 
            flag = 0;
        }
    }
    argv[i][++j] = NULL;
}

void print_d(char*** p) {
    int i,j;
    for(i=0; i<g_pipe+1; i++) {
        printf("command: %s\n", p[i][0] );
        for(j=1; j<MAX_ARG+1; j++) {
            if(p[i][j] == NULL)
                break;
            printf("%s\n", p[i][j]);
        }
    }
}

int fork_child_exec(char** argv, int fd_in, int fd_out, int fd_err, int close_write) {
    int i;
    printf("out in: %d\n", fd_out);
    pid_t p = fork();
    if(p<0) { perror("error occur"); } // should never happen ??
    else if(p==0) { // child
        dup2(fd_in, 0);
        dup2(fd_out, 1);
        if(fd_err!= 0) { dup2(fd_err, 2); }
        // HOW-TO: close global pipe?? 
        for(i=0; i<g_pipe; i++) {
            close(g_local_pipe[i].pipe[0]);
            close(g_local_pipe[i].pipe[1]);
        }
        close(fd[0]);
        close(fd[1]);
        if(execvp(argv[0], argv) == -1) {
            if(errno==ENOENT) { // there may not be printed to console, warning
                printf("Unknown Command: %s\n", argv[0]);
            }
            return -1;
        }
    }
    return 1;
}

void fork_redirct_pipe(int in, int in2, int out) {
    int n;
    pid_t p = fork();
    if(p<0) { perror("error occur"); } // should never happen ??
    else if(p==0) { // child
        dup2(out, 1);
        if(in!=0) {
            dup2(in,0);            
            while(fgets(buf, sizeof(buf), stdin)!=NULL) 
                printf("%s", buf);
        }
        if(in2!=0) {
            if(in!=0) {close(in);}
            dup2(in2, 0);
            while(fgets(buf, sizeof(buf), stdin)!=NULL) 
                printf("%s", buf);
        }
        exit(0);
    }
}

/* TODO: take the g_input_count as para, return global pipe if exist */
int has_global_pipe(int c) {
    return g_pipes[c].is_use ? g_pipes[c].pipe[0] : 0;
}

void fork_redirct_pipe2(int in_pipe, int out_pipe, int target1, int target2) {
    int stat, n;
    n = has_global_pipe(target1);    

    if(n!=0) { // here is really!!! weird!!!!???? why cant we pass three var??
        fork_redirct_pipe(n, 
            0, out_pipe);
        wait(&stat);
        fork_redirct_pipe(0, 
            target2, out_pipe);
    }
    else {
        fork_redirct_pipe(0, 
            target2, out_pipe);
    }

    close(out_pipe);
    close(target2);
    if(n > 0) { close(n); }
    wait(&stat);
    g_pipes[target1].is_use = 1;
    g_pipes[target1].pipe[0] = in_pipe;
    // if(DEBUG) { printf("target1: %d n: %d\n", target1, n); }
}

int has_file_output(char** agv) {
    int i;
    for(i=0; agv[i]!=NULL; i++) {
        if(strcmp(">", agv[i])==0) {
            agv[i] = NULL;
            return i+1;
        }
    }
    return 0;
}

void parse_commands(char* start) {
    char* tok_head=start, *tok_tail;
    char* command_argv[g_pipe+1][MAX_ARG];
    char** argv[g_pipe+1];
    memset(command_argv, '\0', sizeof(command_argv));
    memset(argv, '\0', sizeof(argv));
    int g_pipe_fd[2], g_err_fd[2], first_g_pipe=0, second_g_pipe=0;
    int target1, target2, temp;

    if(g_has_g_pipe>0) {
        if(DEBUG) {printf("global pipe %d\n", g_has_g_pipe);}
        pipe(g_pipe_fd);
        first_g_pipe = g_pipe_fd[0];
        target1 = g_input_count + g_has_g_pipe;
    }
    if(g_has_g_err_pipe>0) {
        if(DEBUG) {printf("global err pope %d\n", g_has_g_err_pipe);}
        pipe(g_err_fd); 
        second_g_pipe = g_err_fd[0];
        target2 = g_input_count + g_has_g_err_pipe;
    }    

    if(global_err_first) {
        first_g_pipe = g_err_fd[0];
        second_g_pipe = g_pipe_fd[0];
        temp = target1;
        target1 = target2;
        target2 = temp;
    }

    if(target1>=1000) { target1 -= 1000; }
    if(target2>=1000) { target2 -= 1000; }

    /* parse command */
    int i,n;
    for(i=0; i<g_pipe+1; i++) {
        tok_tail = find_command_pipe(tok_head);
        if(*tok_tail=='!') { g_local_pipe[i].is_strer = 1; }
        else { g_local_pipe[i].is_strer = 0; }
        argv[i] = command_argv[i];
        parse_command_arg(tok_head, tok_tail, argv, i);
        tok_head = tok_tail+1;
    }

    /* create local pipe */
    for(i=0; i<g_pipe; i++) {
        if(pipe(g_local_pipe[i].pipe) == -1) { perror("error occur"); } // should never occur?
    }

    pipe(fd); // console pipe, if no error then redirect to stdout

    /* execute */
    for(i=0; i<g_pipe+1; i++) {
        int in=0, out=1, err=0;
        // stdin, stdout, stderr
        if(i==0 && ((n=has_global_pipe(g_input_count))>0)) { // TODO
            if(DEBUG) {printf("%d test in n: %d\n", g_input_count , n);}
            in=n; 
        }
        else if(i==0 && (has_global_pipe(g_input_count)==0)) { in = 0; }
        else { in = g_local_pipe[i-1].pipe[0]; }

        // temporary write in a temp pipe, if no error, then redirect to global pipe, and global err pipe
        if(i==g_pipe) { // TODO
            if(g_has_g_pipe>0) { out = g_pipe_fd[1]; }
            else { 
                out = fd[1]; 
                printf("out 1: %d\n", out);
                err = fd[1];
            }
            if(g_has_g_err_pipe>0) { err = g_err_fd[1]; }
        }
        else { 
            if(g_local_pipe[i].is_strer) { err =  g_local_pipe[i].pipe[1]; }
            else { out = g_local_pipe[i].pipe[1]; }
        }

        // TODO: IO redirection
        if((n=has_file_output(argv[i]))>0) { 
            out = open(argv[i][n],"w");
            printf("out 2: %d\n", out);
        }

        // TODO: create global pipe out and global error pipe out 
        // , and then merge to global pipe.
        if(fork_child_exec(argv[i], in, out, err, n)<0) { // TODO error handle (unknow command)
            exit(i+1); // which one is error, child exit here
        }

        if(n>0) { 
            close(out);
            // fork_redirct_pipe(, );
        }
    } 

    if(DEBUG) { print_d(argv); }

    /* close local pipe(write) */
    for(i=0; i<g_pipe; i++) {
        close(g_local_pipe[i].pipe[1]);
    }

    if(g_has_g_pipe>0) { close(g_pipe_fd[1]); }
    if(g_has_g_err_pipe>0) { close(g_err_fd[1]); }
    close(fd[1]);
    /* wait child */
    int min = INT_MAX;
    int stat;
    for(i=0; i<g_pipe+1; i++) {
        pid_t pid;
        pid = wait(&stat);
        stat = WEXITSTATUS(stat);        
        if(DEBUG) {printf("child %d terminated: %d\n", pid, stat);}
        if(stat>0 && stat<min) { // some unknow command happen, find out which one 
            min = stat;
        }
    }

    if(min!=INT_MAX) { // uknow command happen
        // TODO write to the next line command        
        if(DEBUG) { printf("min %d\n", min); }
        printf("Unknown command: %s\n", argv[min-1][0]);
        if(min!=1) { // redirect the last significant pipe to next global pipe
            int temp_pipe[2], next_input_command;
            pipe(temp_pipe);
            next_input_command = g_input_count+1;
            if(next_input_command==1000)
                next_input_command = 0;
            fork_redirct_pipe2(temp_pipe[0], temp_pipe[1], 
                next_input_command, g_local_pipe[min-2].pipe[0]);
            g_input_count++;
            if(g_input_count==1000)
               g_input_count = 0; // reset command counter
        }
        if(first_g_pipe!=0) { close(first_g_pipe); }
        if(second_g_pipe!=0) { close(second_g_pipe); }
    }
    else { // everything works fine, out put to console or global pipe
        /* also dont forget to close global pipe! */
        if((n=has_global_pipe(g_input_count))>0) { close(n); }
        if(g_has_g_pipe>0 || g_has_g_err_pipe>0) {
            int temp_pipe[2], temp_pipe2[2];
            if(first_g_pipe!=0) {
                pipe(temp_pipe);
                fork_redirct_pipe2(temp_pipe[0], temp_pipe[1], 
                    target1, first_g_pipe);
            }            
            if(second_g_pipe!=0) {
                pipe(temp_pipe2);
                fork_redirct_pipe2(temp_pipe2[0], temp_pipe2[1], 
                    target2, second_g_pipe);                
            }
        }
        else { 
            fork_redirct_pipe(fd[0], 0, 1); // stdout, stderr
            printf("here\n");
            wait(&stat);
        }
        g_pipes[g_input_count].is_use = 0;
        g_input_count++;
        if(g_input_count==1000)
           g_input_count = 0; // reset command counter        
    }
    /* close local pipe read */ /* TODO: move this section(hanlde unknow command) */
    for(i=0; i<g_pipe; i++) {
        close(g_local_pipe[i].pipe[0]);
    }    

    close(fd[0]);
    /* also dont forget to close global pipe! */
    if(has_global_pipe(g_input_count)>0) { i=i; } // TODO
    /*  */

    // TODO: global stderr pipe and global pipe
    // TODO: error command pipe still need to redirect
    // TODO: pipe after error command need to close
    // TODO: wait child prevent zombie 
    // TODO: handle command like: cat gg.txt (gg.txt is not exist)
}

void shell() {
    int code;
    printf("[%d] %s", g_input_count,"% " ); // shell prompt, use read and write system call??
    fgets(g_input_command, sizeof(g_input_command), stdin);

    /*  
    ** validate stdout and sterr only appear in the end of a line 
    ** for emaple, "cat hello.txt |1 number |1 number |1" is an invalid command
    */
    if((code=validation(g_input_command)) != 1) {
        printf("Invalid command %d\n", code);
        return;
    }

    parse_commands(g_input_command);
    /* TODO: read from global pipes */

}

void print_env(char** envp) {
    char** env;
    for (env = envp; *env != NULL; env++) {
      char* thisEnv = *env;
      printf("%s\n", thisEnv);
    }
}

int init(char** envp) {
    int i;
    // print_env(envp);
    setenv("PATH", local_bin, 1); // set path
    printf("update: %s\n", getenv("PATH")); // get path
    for(i=0; i<1000; i++) {
        g_pipes[i].is_use = 0;
    }
}

int is_digit(char* p) {
    int num = *p - '0';
    if(num<0 || num>9)
        return 0;
    return 1;
}

int validation(char* start) {
    g_has_g_pipe = g_has_g_err_pipe = global_err_first = g_pipe = 0;
    char* head = start;
    char c = *head;
    char* num_p;
    int g_pipe_num=0;
    for(; *head!='\0'; head++, c=*head) {
        if(c=='|' || c=='!') {
            if(*(head+1) != ' ') { // | or ! should follow by a space, or a number
                if(is_digit(++head)) { // a global pipe, check this number
                    num_p = head;
                    while(is_digit(++head));
                    if(*head!=' ' && *head!='\n') return 2; // invalid coomand
                    *head = '\0';
                    g_pipe_num++; // only accept two consecutive pipes at the end of line
                    if(c=='|') { g_has_g_pipe = atoi(num_p); } // has a global pipe
                    if(c=='!') { // has a global error pipe
                        if(g_pipe_num==1)
                            global_err_first = 1; // only when there are two global pipe, set it
                        g_has_g_err_pipe = atoi(num_p); 
                    }

                }
                else return 3;
            }
            else { // | or ! follow by a new command
                if(g_pipe_num>=1) return 4; // a global pipe should not follow by a command 
                g_pipe++;
            }
        }
        if(g_pipe_num>=3) return 5;
    } 
    if(g_pipe_num==1 && global_err_first==1) { global_err_first = 0; }
    return 1;
}

